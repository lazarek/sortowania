document.getElementById('btnQuickSort').addEventListener('click', function () {

    function quicksort(d, lewy, prawy) {
        var i, j, x;
        i = j = lewy;

        while (i < prawy) {
            if (d[i] <= d[prawy]) { // pivotem jest element o indeksie [prawy]
                x = d[j];
                d[j] = d[i];
                d[i] = x;
                j++;
            }
            i++;
        }

        x = d[j];
        d[j] = d[prawy];
        d[prawy] = x;

        if (lewy < j - 1) quicksort(d, lewy, j - 1);
        if (j + 1 < prawy) quicksort(d, j + 1, prawy);
    } // koniec algorytmu

    var resultArray = array;
    
    time.start();
    quicksort(resultArray, 0, resultArray.length - 1);

    document.getElementById('inputQuickSort').value = resultArray;
    document.getElementById('quickSort').innerHTML = time.stop();

})
